import math
import matplotlib.pyplot as plt


while True:

    # Блок выбора функции
    while True:
        f_name = input('Enter function name: ')
        if f_name in ['G', 'Y', 'F']:
            break
        else:
            print('Incorrect function name')
            pass

    # Блок ввода данных. Проверка значений
    while True:
        try:
            x_start = float(input('Enter first x = '))
            x_end = float(input('Enter second x = '))
            a = float(input('Enter a = '))
            break
        except ValueError:
            print('Value Error')
            pass

    # Блок выбора способа
    while True:
        try:
            x_mode = int(input('Enter 1 for length of step or 2 for number of steps = '))
            if x_mode in (1, 2) and x_mode == 1:
                h = float(input('Enter length of step = '))
                n_step = round(math.fabs(x_end - x_start) / h) + 1
                break
            elif x_mode in (1, 2) and x_mode == 2:
                h = int(input('Enter number of steps = '))
                n_step = h + 1
                break
            else:
                pass
        except ValueError:
            print('Value Error')
            pass

    # Списки x и f
    x = []
    f = []
    for n in range(0, n_step):
        if x_mode == 1:
            x.append(x_start + n * h)
        else:
            x.append(x_start + n * (math.fabs(x_end - x_start) / h))

        # Блок вычислений и вывода результата. Проверка вычислений
        if f_name == 'G':
            try:
                f.append((9 * (7*a**2 - 19*a*x[n] + 10*x[n]**2)) / (25*a**2 + 30*a*x[n] + 9*x[n]**2))
            except ZeroDivisionError:
                f.append(None)
            print(f'Function G = {f[n]:.3f} \t x = {x[n]}')

        elif f_name == 'Y':
            try:
                f.append((math.log(-80*a**2 - 46*a*x[n] + 21*x[n]**2 + 1)) / math.log(10))
            except ValueError:
                f.append(None)
            print(f'Function Y = {f[n]:.3f} \t x = {x[n]}')

        elif f_name == 'F':
            f.append(math.cos(9*a**2 - 13*a*x[n] - 10*x[n]**2))
            print(f'Function F = {f[n]:.3f} \t x = {x[n]}')

    # График
    plt.plot(x, f, 'b*-')
    plt.legend('f(x)')
    plt.title('График функции f(x)')
    plt.xlabel('x --->')
    plt.ylabel('f(x) --->')
    plt.show()

    # Запрос на продолжение работы
    while True:
        p_exit = input('Do you want to continue? (Y/n) ')
        if p_exit not in ['Y', 'y', 'N', 'n']:
            print('Please try again')
            pass
        else:
            break
    if p_exit not in ['Y', 'y']:
        break
    else:
        pass
