import math


# Блок ввода данных
print('Для вычисления функций G, F, Y введите: ')
x = float(input('x = '))
a = float(input('a = '))

# Блок вычисления функций
G = (9*(7*a**2 - 19*a*x + 10*x**2))/(25*a**2 + 30*a*x + 9*x**2)
F = math.cos(9*a**2 - 13*a*x - 10*x**2)
Y = (math.log(-80*a**2 - 46*a*x + 21*x**2 + 1))/math.log(10)

# Блок вывода результата
print('Функция G = {:.5f}'.format(G))
print('Функция F = {:.5f}'.format(F))
print('Функция Y = {:.5f}'.format(Y))
