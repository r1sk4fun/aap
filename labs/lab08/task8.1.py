import lab_module
import random
import matplotlib.pyplot as plt
from timeit import default_timer as timer


i_lst = []
timer_lst = []
while True:
    try:
        radius = int(input('Enter search radius: '))
    except ValueError:
        print('Try again please')
        pass
    break

for i in range(int(10e4), int(10e5), int(10e4)):
    points_lst = [(random.randint(-100, 100), random.randint(-100, 100)) for i in range(i)]
    point_center = (random.randint(-100, 100), random.randint(-100, 100))
    stop_time = 0
    for n in range(3):
        start_time = timer()
        points_count = lab_module.search(points_lst, point_center, radius)
        stop_time += timer() - start_time
    timer_lst.append(stop_time/3)
    i_lst.append(i)

f = open('result.txt', 'w')
for e in timer_lst:
    f.write(f'{e:.5f}\n')
f.close()

plt.plot(i_lst, timer_lst, 'bo-')
plt.xlabel('Number of points')
plt.ylabel('Elapsed time')
plt.show()
