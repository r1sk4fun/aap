import random
import lab_module

while True:
    try:
        points = int(input('Enter number of points:'))
        radius = int(input('Enter radius:'))
    except ValueError:
        print('Try again please')
        pass
    break

points_lst = [(random.randint(-100, 100), random.randint(-100, 100)) for i in range(points)]
point_center = (random.randint(-100, 100), random.randint(-100, 100))

print(f'Number of points:{lab_module.search(points_lst, point_center, radius)}')
