def zone(point, center, radius):
    return ((point[0] - center[0])**2 + (point[1] - center[1])**2) <= radius ** 2


def search(points, center, radius):
    points_count = 0
    for point in points:
        if zone(point, center, radius):
            points_count += 1
    return points_count
