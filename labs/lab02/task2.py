import math


# Блок выбора функции
myfunction = input('Function name: ')
if myfunction in ['G', 'Y', 'F']:
    pass
else:
    exit('Incorrect function name')

# Блок ввода данных. Проверка значений
try:
    x = float(input('Enter x = '))
    a = float(input('Enter a = '))
except ValueError:
    exit('Value error')

# Блок вычислений и вывода результата. Проверка вычислений
if myfunction == 'G':
    try:
        G = (9*(7*a**2 - 19*a*x + 10*x**2)) / (25*a**2 + 30*a*x + 9*x**2)
    except ZeroDivisionError:
        exit('Zero Division Error')
    print('Function G = {:.5f}'.format(G))

elif myfunction == 'Y':
    try:
        Y = (math.log(-80*a**2 - 46*a*x + 21*x**2 + 1)) / math.log(10)
    except ValueError:
        exit('Math Domain Error')
    print('Function Y = {:.5f}'.format(Y))

elif myfunction == 'F':
    F = math.cos(9*a**2 - 13*a*x - 10*x**2)
    print('Function F = {:.5f}'.format(F))
    