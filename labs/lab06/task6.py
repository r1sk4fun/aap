import math
import matplotlib.pyplot as plt


while True:

    # Ввод данных
    while True:
        try:
            x_start = float(input('Enter first x = '))
            x_end = float(input('Enter second x = '))
            a = float(input('Enter a = '))
            h = int(input('Enter number of steps = '))
            break
        except ValueError:
            print('Value Error')
            pass

    # Хранение результатов
    f_dict = {'G': [], 'F': [], 'Y': []}
    x_lst = []

    # Цикл вычислений
    for n in range(0, h + 1):
        x_lst.append(x_start + n * (math.fabs(x_end - x_start) / h))
        try:
            f_dict['G'].append((9*(7*a**2-19*a*x_lst[n]+10*x_lst[n]**2))/(25*a**2+30*a*x_lst[n]+9*x_lst[n]**2))
        except ZeroDivisionError:
            f_dict['G'].append(None)
        try:
            f_dict['F'].append((math.log(-80*a**2-46*a*x_lst[n]+21*x_lst[n]**2+1))/math.log(10))
        except ValueError:
            f_dict['F'].append(None)

        f_dict['Y'].append(math.cos(9*a**2-13*a*x_lst[n]-10*x_lst[n]**2))

    # График
    for k in f_dict:
        plt.plot(x_lst, f_dict[k], 'b*-')
        plt.legend('f(x)')
        plt.title('График функции f(x)')
        plt.xlabel('x --->')
        plt.ylabel('f(x) --->')
        plt.show()
        print(f'{k}: {f_dict[k]}')

    # Запрос на продолжение работы
    while True:
        r_exit = input('Do you want to continue? (Y/n) ')
        if r_exit not in ['Y', 'y', 'N', 'n']:
            print('Please try again')
            pass
        else:
            break
    if r_exit not in ['Y', 'y']:
        break
    else:
        pass
