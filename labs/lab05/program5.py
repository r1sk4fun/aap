while True:
    while True:
        try:
            number = int(input('Enter integer = '))
            break
        except ValueError:
            print(f'Please try again')
            pass

    if number == 0:
        print(f'Number of even numbers = {1}')
        break
    counter = 0

    while number != 0:
        if (number % 10) % 2 == 0:
            counter += 1
        number = number // 10
    print(f'Number of even numbers = {counter}')

    # Запрос на продолжение работы
    while True:
        p_exit = input('Do you want to continue? (Y/n) ')
        if p_exit not in ['Y', 'y', 'N', 'n']:
            print('Please try again')
            pass
        else:
            break
    if p_exit not in ['Y', 'y']:
        break
    else:
        pass
