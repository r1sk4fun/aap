from control_module03 import vector_calc, DifferentLength


vector_x = list(map(int, input("Enter coordinates of X vector with spaces: ").split()))
vector_y = list(map(int, input("Enter coordinates of Y vector with spaces: ").split()))
a = int(input("Enter a: "))
try:
    calculated_vector = vector_calc(vector_x, vector_y, a)
except DifferentLength:
    exit('Different length of vectors')

print(f"Result = {', '.join(calculated_vector)}")
