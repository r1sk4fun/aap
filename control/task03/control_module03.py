class DifferentLength(Exception):
    pass


def vector_calc(vector_x, vector_y, a):
    if len(vector_x) != len(vector_y):
        raise DifferentLength
    else:
        new_vector = [str(x*a+y) for x, y in zip(vector_x, vector_y)]
    return new_vector
