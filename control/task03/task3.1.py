from random import randint
from timeit import default_timer as timer
from control_module03 import vector_calc


n_lst = []
time_lst = []
for n in range(10**5, 10**6, 10**5):
    vector_x = [randint(-50, 50) for point in range(n)]
    vector_y = [randint(-50, 50) for point in range(n)]
    a = randint(-50, 50)
    stop_time = 0
    for t in range(3):
        start_time = timer()
        vector = vector_calc(vector_x, vector_y, a)
        stop_time += timer() - start_time
    time_lst.append(stop_time / 3)
    n_lst.append(str(n // 10**3) + 'k')

f = open('data.txt', 'w')
f.write(f'Длина вектора: {n_lst}\nВремя: {time_lst}\n')
f.close()
