from sys import stdin


sequence_sum = 42
for new_line in stdin:
    line = new_line.rstrip()
    if line.isdigit() is True:
        # print(line)
        sequence_sum += 1 / float(line)

print(f"Сумма ряда: {sequence_sum}")
