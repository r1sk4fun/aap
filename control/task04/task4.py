from control_module04 import matrix_multiply, create_matrix

while True:
    print("Кол-во столбцов в 1 матрице должно быть равно кол-ву строк 2 матрицы")
    row1 = int(input('Введите кол-во строк для 1 матрицы: '))
    column1 = int(input('Введите кол-во столбцов для 1 матрицы: '))
    row2 = int(input('Введите кол-во строк для 2 матрицы: '))
    column2 = int(input('Введите кол-во столбцов для 2 матрицы: '))
    if column1 != row2:
        pass
    else:
        break

print('Введите матрицу №1')
matrix1 = create_matrix(row1, column1)
print('Введите матрицу №2')
matrix2 = create_matrix(row2, column2)

print(f'Матрица №1: {matrix1}\nМатрица №2: {matrix2}')

new_matrix = matrix_multiply(matrix1, matrix2)
print(f'Результат: {new_matrix}')
