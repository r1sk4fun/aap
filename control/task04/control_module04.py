def matrix_multiply(m1, m2):
    s = 0
    time_matrix = []
    new_matrix = []
    for n in range(0, len(m1)):
        for j in range(0, len(m1[0])):
            for i in range(0, len(m2[0])):
                s += m1[n][i] * m2[i][j]
            time_matrix.append(s)
            s = 0
        new_matrix.append(time_matrix)
        time_matrix = []
    return new_matrix


def create_matrix(rows, column):
    matrix = []
    for i in range(0, rows):
        time_matrix = []
        for j in range(0, column):
            time_matrix.append(float(input(f'Enter matrix[{i}][{j}]:\t')))
        matrix.append(time_matrix)
    return matrix
