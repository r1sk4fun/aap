from random import randint
from timeit import default_timer as timer
from control_module04 import matrix_multiply


n_lst = []
time_lst = []
for n in range(10**3, 10**5, 10**4):
    matrix_a = [[randint(1, 10) for j in range(int(n ** 0.5))] for i in range(int(n ** 0.5))]
    matrix_b = [[randint(1, 10) for j in range(int(n ** 0.5))] for i in range(int(n ** 0.5))]
    stop_time = 0
    for t in range(5):
        start_time = timer()
        matrix_multiply(matrix_a, matrix_b)
        stop_time += timer() - start_time
    time_lst.append(stop_time / 5)
    n_lst.append(n)

f = open('data.txt', 'w')
f.write(f'Размерность матрицы: {n_lst}\nВремя: {time_lst}\n')
f.close()
