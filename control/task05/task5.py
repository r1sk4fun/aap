from control_module05 import create_matrix, gauss, cramer, InfinitySolutionsError

rows = int(input('Enter number of rows: '))
matrix = create_matrix(rows, rows + 1)
print(matrix)

method = int(input("Choose method 1 - Gauss-Jordan or 2 - Cramer: "))
if method == 1:
    try:
        result = gauss(rows, matrix)
    except ZeroDivisionError:
        exit('Zero division')
else:
    try:
        result = cramer(rows, matrix)
    except InfinitySolutionsError:
        exit('Infinity number of solutions')

for i in range(rows):
    print(f"X{i} = {result[i]}", end=';\t')
