from random import randint
from timeit import default_timer as timer
from control_module05 import gauss, cramer, InfinitySolutionsError


stop_time = 0
n_lst = []
time_lst_1 = []
time_lst_2 = []

for n in range(10, 100, 10):
    matrix = []
    for i in range(n):
        time_matrix = []
        for j in range(n + 1):
            time_matrix.append(randint(1, 11))
        matrix.append(time_matrix.copy())
    n_lst.append(n)

    for _ in range(3):
        start_time = timer()
        result = gauss(n, matrix)
        stop_time += timer() - start_time
    time_lst_1.append(stop_time / 3)
    stop_time = 0

    for _ in range(3):
        start_time = timer()
        try:
            result = cramer(n, matrix)
        except InfinitySolutionsError:
            exit('Infinity number of solutions')
        stop_time += timer() - start_time
    time_lst_2.append(stop_time / 3)
    stop_time = 0

f = open('data.txt', 'w')
f.write(f'Dimension: {n_lst}\nGauss: {time_lst_1}\nCramer: {time_lst_2}\n')
f.close()
