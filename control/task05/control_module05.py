from copy import deepcopy


def create_matrix(rows, column):
    matrix = []
    for i in range(0, rows):
        time_matrix = []
        for j in range(0, column):
            time_matrix.append(float(input(f'Enter matrix[{i}][{j}]:\t')))
        matrix.append(time_matrix)
    return matrix


def gauss(rows, matrix):
    for i in range(rows):
        for j in range(rows):
            if i != j:
                ratio = matrix[j][i] / matrix[i][i]
                for k in range(rows + 1):
                    matrix[j][k] = matrix[j][k] - ratio * matrix[i][k]
    result = []
    for i in range(rows):
        vector_coord = round(matrix[i][rows] / matrix[i][i], 2)
        result.append(vector_coord)
    return result


def zeros_matrix(rows, cols):
    zero_matrix = []
    while len(zero_matrix) < rows:
        zero_matrix.append([])
        while len(zero_matrix[-1]) < cols:
            zero_matrix[-1].append(0.0)
    return zero_matrix


def copy_matrix(matrix):
    # Section 1: Get matrix dimensions
    rows = len(matrix)
    cols = len(matrix[0])

    # Section 2: Create a new matrix of zeros
    new_matrix = zeros_matrix(rows, cols)

    # Section 3: Copy values of matrix into the copy
    for i in range(rows):
        for j in range(cols):
            new_matrix[i][j] = matrix[i][j]
    return new_matrix


def determinant_fast(matrix):
    n = len(matrix)
    matrix_copy = copy_matrix(matrix)

    # Section 2: Row manipulate matrix into an upper triangular matrix
    for fd in range(n):  # fd stands for focus diagonal
        if matrix_copy[fd][fd] == 0:
            matrix_copy[fd][fd] = 1.0e-18  # Cheating by adding zero + ~zero
        for i in range(fd+1, n):  # skip row with fd in it.
            cr_s = matrix_copy[i][fd] / matrix_copy[fd][fd]
            for j in range(n):
                matrix_copy[i][j] = matrix_copy[i][j] - cr_s * matrix_copy[fd][j]

    # Section 3: Once matrix_copy is in upper triangular form ...
    product = 1.0
    for i in range(n):
        product *= matrix_copy[i][i]  # ... product of diagonals is determinant
    return product


class InfinitySolutionsError(Exception):
    pass


def cramer(rows, matrix):
    matrix_determinant = determinant_fast(matrix)
    if matrix_determinant == 0:
        raise InfinitySolutionsError
    result = []
    for i in range(rows):
        new_matrix = deepcopy(matrix)
        for j in range(rows):
            new_matrix[j][i] = matrix[j][-1]
        new_matrix_determinant = round(determinant_fast(new_matrix) / matrix_determinant, 2)
        result.append(new_matrix_determinant)
    return result
